#include"Arbol.h"

using namespace std;

Estudiante crear_fulanitos(){
    float promedio;
    string matricula;

    cout << "ingrese promedio" << endl;
    while(!(cin >> promedio)){
        cout << "Debe ser un numero: ";
        cin.clear();
        cin.ignore(100, '\n');
    }
    cout << "ingrese matricula" << endl;
    cin >> matricula;
    Estudiante fulanito(promedio, matricula);
    return fulanito;
}

int main(){

    Arbol arbol;
    while (true){
        cout << "\n\n1 = añador datos" << endl;
        cout << "2 = ordenar pre order" << endl;
        cout << "3 = ordenar post order" << endl;
        cout << "4 = ordenar in order" << endl;
        cout << "5 =finalizar programa" << endl;
        cout << "ingrese su opcion:" << endl;
        int opcion;
        cin >> opcion;
        switch (opcion)
        {
        case 1:
            arbol.Agregar(crear_fulanitos());
            break;

        case 2:
            arbol.pre_order();
            break;

        case 3:
            arbol.post_order();
            break;

        case 4:
            arbol.in_order();
            break;

        case 5:
            return 0;

        default:
            break;
        }
    }

    return 0;
}