#include<iostream>

using namespace std;

class Estudiante{

    private:
        float promedio;
        string matricula;
        friend class Nodo;

    public:
        Estudiante(float prom, string mat):promedio(prom), matricula(mat){}
        float get_promedio(){
            return promedio;
        }
        string get_matricula(){
            return matricula;
        }

};