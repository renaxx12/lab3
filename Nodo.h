#include"Estudiante.h"

using namespace std;

class Nodo{

    private:
        Nodo *nodo_izq;
        Nodo *nodo_derecha;
        Estudiante valor;
        friend class Arbol;
    
    public:
        Nodo(Estudiante x): valor(x), nodo_derecha(NULL), nodo_izq(NULL){}

        Nodo *get_izq(){
            return nodo_izq;
        }
        Nodo *get_derecha(){
            return nodo_derecha;
        }
        Estudiante *get_valor(){
            return &valor;
        }
};