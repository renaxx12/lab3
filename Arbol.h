#include"Nodo.h"

using namespace std;

class Arbol{

    private:
        Nodo *raiz;

        Nodo *insertar(Nodo *root, Estudiante dato) {
            if (root == NULL)
                { 
                  return new Nodo(dato);
                }

            else {
                if (dato.get_promedio() >= root->get_valor()->get_promedio()) 
                    root->nodo_izq = insertar(root->nodo_izq, dato);
                else 
                    root->nodo_derecha = insertar(root->nodo_derecha, dato);
                
            }
            
            return root;
        }

        void ordenar_in_order(Nodo *nodo){
            if (nodo == NULL){
                return;
            }
            ordenar_in_order(nodo->nodo_izq);
            cout << nodo->valor.get_promedio() << " | ";
            ordenar_in_order(nodo->nodo_derecha);
        }
        void ordenar_post_order(Nodo *nodo){
            if (nodo == NULL){
                return;
            }
            ordenar_post_order(nodo->nodo_izq);
            ordenar_post_order(nodo->nodo_derecha);
            cout << nodo->valor.get_promedio() << " | ";
        }
        void ordenar_pre_order(Nodo *nodo){
            if (nodo == NULL){
                return;
            }
            cout << nodo->valor.get_promedio() << " | ";
            ordenar_pre_order(nodo->nodo_izq);
            ordenar_pre_order(nodo->nodo_derecha);
        }

    public:

        Arbol(): raiz(NULL){}

        void Agregar(Estudiante valor){
            raiz = insertar(raiz, valor);    
        }

        void in_order(){
            ordenar_in_order(raiz);
        }
        void post_order(){
            ordenar_post_order(raiz);
        }
        void pre_order(){
            ordenar_pre_order(raiz);
        }
};